= Client side in browser compiling and rendering

Both compilation and rendering are made in the browser: templates can be authored directly in the browser, or dynamically generated, compiled in the browser and the texts can also be rendered in the browser.

To be able to do that, you must use a js version of RosaeNLG that is larger, with `_comp` at the end, like `rosaenlg_tiny_fr_FR_*_comp.js`:

* it includes the templates compiler
* but also all linguistic resources that are associated with a language

TIP: Client side compilation is for exotic usecases. You should favor server side compilation.

It can be used to create a 100% client side editor and renderer, link:https://rosaenlg.org/ide/index.html[like in this demo].

Works with Firefox and Edge, but not with older IE.

== Packaged version of RosaeNLG for client side usage

RosaeNLG provides browser ready "tinyified" packages per language in `dist/browser`:

.Available packages per language
[options="header"]
|=======================================================
| Language | Rendering only | With compilation
| `en_US` | 360 Kb | 1.7 Mb
| `fr_FR` | 275 Kb | 8.7 Mb
| `de_DE` | 249 Kb | 44 Mb
| `it_IT` | 259 Kb | 10.1 Mb
| `es_ES` | 257 Kb | 1.5 Mb
| `OTHER` | 211 Kb | 1.5 Mb
|=======================================================

`en_US` rendering only version is a little larger as it includes `a/an` linguistic resource.

CAUTION: Well, the German version with compilation support is too large indeed.

== CDN

All versions are available on `unpkg.com` CDN, for instance:
https://unpkg.com/rosaenlg@1.8.0/dist/rollup/rosaenlg_tiny_en_US_1.8.0_comp.js
https://unpkg.com/rosaenlg@1.8.0/dist/rollup/rosaenlg_tiny_en_US_1.8.0.js
