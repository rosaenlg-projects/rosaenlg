# RosaeNLG Command Line Interface

CLI interface for RosaeNLG.

Is originally a fork from [Pug CLI](https://github.com/pugjs/pug-cli).

For documentation, see [RosaeNLG documentation](https://rosaenlg.org).
