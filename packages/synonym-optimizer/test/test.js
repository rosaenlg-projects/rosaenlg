const lib = require('../dist/index.js');
const assert = require('assert');

const extractedWordsPerLang = {
  // eslint-disable-next-line @typescript-eslint/naming-convention
  fr_FR: {
    "Bonjour. Je suis très content, j'ai mangé une bonne salade!!!": [
      'Bonjour',
      'Je',
      'suis',
      'très',
      'content',
      'ai',
      'mangé',
      'une',
      'bonne',
      'salade',
    ],
    'bla.bla': ['bla', 'bla'],
    '... et : alors!': ['et', 'alors'],
    '<div>bla <b>bla</b><div>': ['bla', 'bla'],
    '<p><toto>bla</toto></p>': ['toto', 'bla', 'toto'],
    "j'ai mangé je n'ai pas t'as vu": ['ai', 'mangé', 'je', 'ai', 'pas', 'as', 'vu'],
  },
  // eslint-disable-next-line @typescript-eslint/naming-convention
  es_ES: {
    'Hago una prueba': ['Hago', 'una', 'prueba'],
  },
};

const stemmedFiltered = [
  ['fr_FR', 'absolument constitutionnel bouffé', ['constitutionnel', 'bouff']],
  ['es_ES', 'absolutamente constitucional aguas', ['absolut', 'constitucional', 'agu']],
];

const wordsWithPos = [
  [
    'fr_FR',
    ['bla', 'alors', 'bla', 'bli', 'xxx', 'xxx', 'yyy'],
    null,
    { bla: [0, 2], alors: [1], bli: [3], xxx: [4, 5], yyy: [6] },
  ],
  ['fr_FR', ['bla', 'bla', 'je', 'ai', 'bla'], null, { bla: [0, 1, 4], je: [2], ai: [3] }],
  ['en_US', ['bla', 'bli', 'blu'], null, { bla: [0], bli: [1], blu: [2] }],
  ['en_US', ['bla', 'bli', 'blu'], [['bla', 'blu']], { bla_blu: [0, 2], bli: [1] }], // eslint-disable-line
  ['it_IT', ['azzurra', 'cameriere'], null, { azzurra: [0], cameriere: [1] }],
  ['nl_NL', ['slipje', 'bokser', 'snaar'], null, { slipje: [0], bokser: [1], snaar: [2] }],
  ['nl_NL', ['slipje', 'bokser', 'snaar'], [['slipje', 'bokser']], { slipje_bokser: [0, 1], snaar: [2] }], // eslint-disable-line
];

const scores = [
  [{ bla: [0, 1, 4], je: [2], ai: [3, 5, 6] }, 2.83],
  [{ bla: [0], bli: [1], blu: [2] }, 0],
];

const globalTests = [
  ['fr_FR', ['bla bla bla', 'bli bla bla'], 1],
  ['fr_FR', ['bla bli bla', 'bla bla bli'], 0],
];

const scoreAlternativeTests = [
  ['en_US', 'arms arm', 1],
  ['en_US', 'diamonds diamond', 1],
  ['en_US', 'he eats they eat', 1],
  ['en_US', 'I engineered I engineer', 1],
  ['fr_FR', 'bonjour test', 0],
  ['fr_FR', 'poubelle alors alors alors poubelles', 1],
  ['fr_FR', 'allée allé', 1],
  ['de_DE', 'katholik katholische katholischen', 2],
  ['it_IT', 'azzurra cameriere', 0],
  ['it_IT', 'azzurra azzurra', 1],
  ['it_IT', 'azzurra azzurro azzurri', 2],
  ['it_IT', 'azzurra azzurro azzurri azzurre cameriere', 3],
  ['it_IT', 'camerieri cameriera', 1],
  ['it_IT', 'azzurra azzurro azzurri azzurre camerieri cameriera', 4],
  ['es_ES', 'agua agua', 1],
  ['es_ES', 'agua aguas', 1],
  ['nl_NL', 'slipje bokser snaar', 0],
  ['nl_NL', 'slipje slipje slipje', 2],
  ['nl_NL', 'slipje slipje slips', 1],
  ['ja_JP', '本当に暑いです', 0], // doesn't work at all, but should not fail
];

describe('synonym-optimizer', function () {
  describe('#getStandardStopWords', function () {
    it('alors / fr', function () {
      assert(lib.getStandardStopWords('fr_FR').includes('alors'));
    });
    it('entonces / es', function () {
      assert(lib.getStandardStopWords('es_ES').includes('entonces'));
    });
    it('void if new language', function () {
      assert(lib.getStandardStopWords('nl_NL').length === 0);
    });
  });

  describe('#getStopWords', function () {
    it('specific list', function () {
      assert.deepEqual(lib.getStopWords(null, null, null, ['xx', 'yy']), ['xx', 'yy']);
    });
    it('remove', function () {
      assert(!lib.getStopWords('fr_FR', null, 'alors', null).includes('alors'));
    });
    it('add', function () {
      assert(lib.getStopWords('fr_FR', ['blabla'], null, null).includes('blabla'));
    });
    it('new language', function () {
      assert(lib.getStopWords('nl_NL', null, null, null).length === 0);
      assert(lib.getStopWords('nl_NL', ['de', 'een'], null, null).includes('een'));
    });
  });

  //  const stemmedFiltered = [['fr_FR', 'absolument constitutionnel bouffé', ['absolument', 'constitutionnel', 'bouff']]];
  // getStemmedWords(text: string, stopwords: string[], lang: Languages): string[] {
  describe('#getStemmedWords', function () {
    for (let i = 0; i < stemmedFiltered.length; i++) {
      const testCase = stemmedFiltered[i];
      const lang = testCase[0];
      const input = testCase[1];
      const expected = testCase[2];
      it(`${lang} ${input} => ${expected}`, function () {
        assert.deepEqual(lib.getStemmedWords(input, lib.getStopWords(lang), lang), expected);
      });
    }
  });

  describe('#extractWords', function () {
    for (const lang in extractedWordsPerLang) {
      describe(lang, function () {
        const cases = extractedWordsPerLang[lang];
        Object.keys(cases).forEach(function (key) {
          const vals = cases[key];
          it(`${key} => ${JSON.stringify(vals)}`, function () {
            assert.deepEqual(lib.extractWords(key, 'fr_FR'), vals);
          });
        });
      });
    }
  });

  describe('#getWordsWithPos', function () {
    describe('nominal', function () {
      wordsWithPos.forEach(function (testCase) {
        const lang = testCase[0];
        const input = testCase[1];
        const identicals = testCase[2];
        const expected = testCase[3];
        it(`${input}`, function () {
          assert.deepEqual(lib.getWordsWithPos(lang, input, identicals), expected);
        });
      });
    });

    describe('edge', function () {
      it(`identicals not string[]`, function () {
        assert.throws(() => lib.getWordsWithPos('en_US', ['bla'], 'bla'), /string/);
      });
      it(`identicals not string[][]`, function () {
        assert.throws(() => lib.getWordsWithPos('en_US', ['bla'], ['bla']), /string/);
      });
    });
  });

  describe('#getScore', function () {
    scores.forEach(function (testCase) {
      const input = testCase[0];
      const expected = testCase[1];
      it(`${JSON.stringify(input)} => ~${expected}`, function () {
        assert(Math.abs(lib.getScore(input) - expected) < 0.01);
      });
    });
  });

  describe('#getBest', function () {
    globalTests.forEach(function (testCase) {
      const lang = testCase[0];
      const input = testCase[1];
      const expected = testCase[2];
      it(`some test in ${lang} => ${expected}`, function () {
        assert.equal(lib.getBest(lang, input, null, null, null, null, null), expected);
      });
    });
  });

  describe('#scoreAlternative', function () {
    scoreAlternativeTests.forEach(function (testCase) {
      const language = testCase[0];
      const input = testCase[1];
      const expectedScore = testCase[2];

      it(`${language} ${input} => ${expectedScore}`, function () {
        const debugHolder = {};
        const score = lib.scoreAlternative(language, input, null, null, null, null, debugHolder);
        //console.log(debugHolder);
        assert.equal(score, expectedScore);
      });
    });

    it(`with debug`, function () {
      const debug = {};
      lib.scoreAlternative('fr_FR', 'AAA AAA', null, null, null, null, debug);
      assert.equal(debug.score, 1);
    });

    const forIdenticalsTest = 'phone cellphone smartphone bla bla';
    it(`identicals - without`, function () {
      assert.equal(lib.scoreAlternative('fr_FR', forIdenticalsTest, null, null, null, null, null), 1);
    });
    it(`identicals - with`, function () {
      const debugHolder = {};
      const score = lib.scoreAlternative(
        'fr_FR',
        forIdenticalsTest,
        null,
        null,
        null,
        [['phone', 'cellphone', 'smartphone']],
        debugHolder,
      );
      //console.log(debugHolder);
      assert.equal(score, 3);
    });

    /*
    it(`invalid language`, function() {
      assert.throws(() => lib.scoreAlternative('latin', 'bla', null, null, null, null, null), /language/);
    });
    */
  });
});
