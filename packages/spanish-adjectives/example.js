const SpanishAdjectives = require('./dist/index.js');

// negras
console.log(SpanishAdjectives.agreeAdjective('negro', 'F', 'P'));

// daneses
console.log(SpanishAdjectives.agreeAdjective('danés', 'M', 'P'));
