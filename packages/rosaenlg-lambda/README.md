# RosaeNLG Lambda

Lambda functions for RosaeNLG to deploy on AWS Lambda.

For documentation, see [RosaeNLG main documentation](https://rosaenlg.org).

