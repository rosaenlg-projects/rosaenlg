export { Constants, Languages } from './constants';
export { DictManager } from './DictManager';
export {
  isConsonneImpure as italianIsConsonneImpure,
  isIFollowedByVowel as italianIsIFollowedByVowel,
  startsWithVowel as italianStartsWithVowel,
} from './italian';
