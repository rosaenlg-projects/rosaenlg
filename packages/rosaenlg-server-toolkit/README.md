# RosaeNLG Server Toolkit

Common components for `rosaenlg-node-server` and `rosaenlg-lambda`.

For documentation, see [RosaeNLG main documentation](https://rosaenlg.org).
