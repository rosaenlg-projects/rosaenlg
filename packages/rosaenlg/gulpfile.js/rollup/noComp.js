const nlgLib = require('../../dist/NlgLib');

module.exports = {
  NlgLib: nlgLib.NlgLib,
  getRosaeNlgVersion: nlgLib.getRosaeNlgVersion,
};

exports.NlgLib = nlgLib.NlgLib;
exports.getRosaeNlgVersion = nlgLib.getRosaeNlgVersion;
