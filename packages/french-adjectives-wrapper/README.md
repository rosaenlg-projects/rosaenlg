# french-adjectives-wrapper

Technical wrapper on `french-adjectives`, tailored for `RosaeNLG`.
You should use `french-adjectives` directly.
